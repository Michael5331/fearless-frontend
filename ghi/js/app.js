function createCard(name, description, location, dates,  pictureUrl) {
    return `
        <div class="card shadow m-3">
            <div class="card-body">
                <img src="${pictureUrl}" class="card-img-top">
                <h3 class="card-title h5">${name}</h3>
                <h4 class="text-muted h6">${location}</h4>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${dates}
            </div>
        </div>

    `;
  }


function formatDates(detail) {
    const start = new Date(detail.conference.starts).toLocaleDateString();
    const end = new Date(detail.conference.ends).toLocaleDateString();
    return `${start} - ${end}`
}

window.addEventListener('DOMContentLoaded', async () => {


    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {

        } else {

            const data = await response.json();

            for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const location = details.conference.location.name;
                const dates = formatDates(details);
                const html = createCard(name, description, location, dates,  pictureUrl);
                const column = document.querySelector('.card');
                column.innerHTML += html;
            }
        }

    }
    } catch (e) {
        console.error(e);
    }


});
